import random



list_odd_numbers = [1, 3, 5, 7, 9, 13]
list_not_odd_numbers = [2, 4, 8, 10]

def generation_random_list():
    generation_list = []
    for i in range(1, 10000000, 2):
        generation_list.append(int(i))
    random_removed_number = generation_list.pop(random.randint(0, len(generation_list)))
    print(random_removed_number)
    return(generation_list)

result_list = generation_random_list()


def search_missed_number(list_numbers):
    max_number_in_list = max(list_numbers)
    our_sum_progressions = sum(list_numbers)
    if max_number_in_list % 2 == 1:
        quantity_number = (max_number_in_list+1)/2
        sum_progressions = (1+max_number_in_list)/2*quantity_number
        our_number = sum_progressions - our_sum_progressions
        print(int(our_number))
        return
    else:
        quantity_number = max_number_in_list /2
        sum_progressions = (2 + max_number_in_list) / 2 * quantity_number
        our_number = sum_progressions - our_sum_progressions
        print(int(our_number))
        return



search_missed_number(list_odd_numbers)

search_missed_number(list_not_odd_numbers)



search_missed_number(result_list)